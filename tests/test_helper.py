import logging
from datetime import datetime
from unittest import TestCase

from glsf.helper import json_serial
from glsf.helper import map_log_level
from glsf.helper import to_list
from glsf.helper import to_string


class TestHelper(TestCase):
    def __init__(self, *args, **kwargs):
        self.test_csv = "A,B,C,D"
        self.test_list = ["A", "B", "C", "D"]
        super().__init__(*args, **kwargs)

    def test_json_serial(self):
        class Foo:
            def __init__(self):
                self.x = 1234

        class NoDict:
            __slots__ = ()

        d = datetime(2018, 11, 11, 11, 11, 11)
        self.assertIsInstance(json_serial(d), str)
        self.assertIsInstance(json_serial(Foo()), dict)
        with self.assertRaises(TypeError):
            json_serial(NoDict())

    def test_to_string(self):
        self.assertEqual(self.test_csv, to_string(self.test_list))

    def test_to_list(self):
        self.assertEqual(self.test_list, to_list(self.test_csv))

    def test_map_log_level(self):
        self.assertEqual(map_log_level(True), logging.DEBUG)
        self.assertEqual(map_log_level(False), logging.ERROR)
        self.assertEqual(map_log_level(0), logging.ERROR)
        self.assertEqual(map_log_level(5), logging.DEBUG)
        self.assertEqual(map_log_level(2), logging.INFO)
