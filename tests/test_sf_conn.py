from unittest import mock
from unittest import TestCase

from glsf.sf_conn import retry_on_session_expire
from glsf.sf_conn import SingletonSalesforce

# import simple_salesforce
# from simple_salesforce import SalesforceAuthenticationFailed
# from glsf.sf_conn import get_sf_object


class TestEngage(TestCase):
    def __init__(self, *args, **kwargs):
        self.cfg_json = {
            "sf_username": "x",
            "sf_password": "foo",
            "sf_token": "vUVwdgATcZjTCNjXTi4cwI7c",
            "sf_instance": "test.foo.bar",
        }
        super().__init__(*args, **kwargs)

    #
    # def test_get_sf_object(self):
    #     with mock.patch('simple_salesforce.Salesforce', autospec=True) as msf:
    #         print("-"*30)
    #         print(get_sf_object(self.cfg_json))
    #         print("-"*30)
    # with mock.patch("simple_salesforce.SalesForce") as msf:
    #     mock_sf = msf.return_value
    #     print(mock_sf)
    #
    # with self.assertRaises(SalesforceAuthenticationFailed):
    # self.assertIsNone(get_sf_object(self.cfg_json))

    def test_engage_singleton_behaviour(self):
        with mock.patch("glsf.sf_conn.get_sf_object"):
            sf_instance1 = SingletonSalesforce("x")
            sf_instance2 = SingletonSalesforce("x")
            self.assertEqual(sf_instance1.sf, sf_instance2.sf)

    def test_destroy_instance_remove_sf_instance(self):
        with mock.patch("glsf.sf_conn.get_sf_object"):
            SingletonSalesforce(1)
            self.assertTrue(hasattr(SingletonSalesforce, "_instance"))
            SingletonSalesforce.destroy_instance()
            self.assertFalse(hasattr(SingletonSalesforce, "_instance"))


class TestSimpleSalesForceDecorators(TestCase):
    @retry_on_session_expire
    def find_users(self):
        engage = SingletonSalesforce(1)
        return engage.sf.search("FIND {Bruce}")

    def test_expired_session_refreshed_once(self):
        with mock.patch("glsf.sf_conn.get_sf_object"):
            engage = SingletonSalesforce(1)
            engage.sf.session_id = "xxx"
            engage.sf.headers["Authorization"] = "xxx"
            users = self.find_users()
            self.assertTrue(users.get("searchRecords"))

    @retry_on_session_expire
    def find_users_expired_session(self):
        engage = SingletonSalesforce(1)
        engage.sf.session_id = "xxx"
        engage.sf.headers["Authorization"] = "xxx"
        return engage.sf.search("FIND {Bruce}")

    # COMMENTING THIS OUT AS IT REQUIRES A REAL SESSION.
    # def test_expired_session_refreshed_only_once(self):
    #     with self.assertRaises(SalesforceExpiredSession) as error:
    #         users = self.find_users_expired_session()
    #         self.assertTrue(users.get('searchRecords'))
