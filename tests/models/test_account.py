import json
import logging
from collections import OrderedDict
from unittest import mock
from unittest import TestCase

from glsf.models.account import Account
from glsf.models.account import AccountService

# import simple_salesforce.api
# from glsf import models
# from glsf.sf_conn import SingletonSalesforce

logger = logging.getLogger(__name__)

# from src import glsf
# import models.user


class TestAccount(TestCase):
    def __init__(self, *args, **kwargs):
        self.account_json = {
            "Id": "sado8f698a32r4uk",
            "Name": "GitLab",
            "Description": "GitLab, Inc.",
            "LicenseCount": "2000",
            "LicenseTier": "Ultimate",
            "Owner": "123",
            "SA": "321",
        }
        self.account_obj = x = json.loads(
            json.dumps(self.account_json), object_hook=lambda d: Account(**d)
        )
        self.search_results = [self.account_obj]
        self.mock_account_json = OrderedDict(
            [
                (
                    "attributes",
                    OrderedDict(
                        [
                            ("type", "Account"),
                            (
                                "url",
                                "/services/data/v52.0/sobjects/Account/{}".format(
                                    self.account_json["Id"]
                                ),
                            ),
                        ]
                    ),
                ),
                ("Id", self.account_json["Id"]),
                ("Name", self.account_json["Name"]),
                ("Description", self.account_json["Description"]),
                ("Number_of_Licenses_Total__c", self.account_json["LicenseCount"]),
                ("Products_Purchased__c", self.account_json["LicenseTier"]),
                ("OwnerId", self.account_json["Owner"]),
                ("Solutions_Architect_Lookup__c", self.account_json["SA"]),
            ]
        )
        super().__init__(*args, **kwargs)

    def test_load(self):
        with mock.patch("simple_salesforce.api.SFType") as msf:
            mock_sf = msf.return_value
            mock_sf.Account.get.return_value = self.mock_account_json
            a_serv = AccountService(mock_sf)
            a = a_serv.load(self.account_json["Id"])
            assert isinstance(a, Account)
            assert a.id == self.account_obj.id
            assert a.name == self.account_obj.name
            assert a.description == self.account_obj.description
            assert a.license_count == self.account_obj.license_count
            assert a.license_tier == self.account_obj.license_tier
            assert a.owner == self.account_obj.owner
            assert a.sa == self.account_obj.sa
        return True
