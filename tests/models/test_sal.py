import json
import logging
from collections import OrderedDict
from unittest import mock
from unittest import TestCase

from glsf.models.sal import SAL
from glsf.models.sal import SALService

# import simple_salesforce.api
# from glsf import models

# from glsf.sf_conn.api import SingletonSalesforce

logger = logging.getLogger(__name__)

# from src import glsf
# import models.user


class TestSal(TestCase):
    def __init__(self, *args, **kwargs):
        self.sal_json = {
            "id": "sado8f698a32r4uk",
            "first_name": "Bruce",
            "last_name": "Banner",
            "email": "bbanner@gitlab.com",
        }
        self.sal_obj = x = json.loads(
            json.dumps(self.sal_json), object_hook=lambda d: SAL(**d)
        )
        self.search_results = [self.sal_obj]
        self.mock_sal_json = OrderedDict(
            [
                (
                    "attributes",
                    OrderedDict(
                        [
                            ("type", "User"),
                            (
                                "url",
                                "/services/data/v52.0/sobjects/User/{}".format(
                                    self.sal_json["id"]
                                ),
                            ),
                        ]
                    ),
                ),
                ("Id", self.sal_json["id"]),
                ("FirstName", self.sal_json["first_name"]),
                ("LastName", self.sal_json["last_name"]),
                ("Email", self.sal_json["email"]),
            ]
        )
        self.mock_query_results = OrderedDict(
            [
                ("totalSize", 1),
                ("done", True),
                (
                    "records",
                    [
                        OrderedDict(
                            [
                                (
                                    "attributes",
                                    OrderedDict(
                                        [
                                            ("type", "User"),
                                            (
                                                "url",
                                                "/services/data/v52.0/sobjects/User/{}".format(
                                                    self.sal_json["id"]
                                                ),
                                            ),
                                        ]
                                    ),
                                ),
                                ("Id", self.sal_json["id"]),
                                ("FirstName", self.sal_json["first_name"]),
                                ("LastName", self.sal_json["last_name"]),
                                ("Email", self.sal_json["email"]),
                            ]
                        )
                    ],
                ),
            ]
        )
        super().__init__(*args, **kwargs)

    def test_load(self):
        with mock.patch("simple_salesforce.api.SFType") as msf:
            mock_sf = msf.return_value
            mock_sf.User.get.return_value = self.mock_sal_json
            ss = SALService(mock_sf)
            s = ss.load(self.sal_json["id"])
            assert isinstance(s, SAL)
            assert s.id == self.sal_obj.id
            assert s.first_name == self.sal_obj.first_name
            assert s.last_name == self.sal_obj.last_name
            assert s.email == self.sal_obj.email
        return True

    def test_search(self):
        with mock.patch("simple_salesforce.api.Salesforce") as msf:
            mock_sf = msf.return_value
            mock_sf.query.return_value = {"records": [self.mock_sal_json]}
            ss = SALService(mock_sf)
            rs = ss.search(first_name="Bruce")
            # assert isinstance(rs[0], SAL)
            assert rs == self.search_results
            rs = ss.search(last_name="Banner")
            assert rs == self.search_results
            rs = ss.search(first_name="Bruce", last_name="Banner")
            assert rs == self.search_results
            rs = ss.search(email="bbanner@gitlab.com")
            assert rs == self.search_results
        with self.assertRaises(ValueError):
            ss.search()
