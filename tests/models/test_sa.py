import json
from collections import OrderedDict
from unittest import mock
from unittest import TestCase

from glsf.models.sa import SA
from glsf.models.sa import SAService

# import simple_salesforce.api

# from glsf.models.user import User as sa_model
# from glsf.sf_conn.api import SingletonSalesforce

# from src import glsf
# import models.user


class TestSA(TestCase):
    def __init__(self, *args, **kwargs):
        self.sa_json = {
            "id": "sado8f698a32r4uk",
            "first_name": "Bruce",
            "last_name": "Banner",
            "email": "bbanner@gitlab.com",
        }
        self.sa_obj = x = json.loads(json.dumps(self.sa_json), object_hook=lambda d: SA(**d))
        self.search_results = [self.sa_obj]
        self.mock_sa_json = OrderedDict(
            [
                (
                    "attributes",
                    OrderedDict(
                        [
                            ("type", "User"),
                            (
                                "url",
                                "/services/data/v52.0/sobjects/User/{}".format(
                                    self.sa_json["id"]
                                ),
                            ),
                        ]
                    ),
                ),
                ("Id", self.sa_json["id"]),
                ("FirstName", self.sa_json["first_name"]),
                ("LastName", self.sa_json["last_name"]),
                ("Email", self.sa_json["email"]),
            ]
        )
        self.mock_query_results = OrderedDict(
            [
                ("totalSize", 1),
                ("done", True),
                (
                    "records",
                    [
                        OrderedDict(
                            [
                                (
                                    "attributes",
                                    OrderedDict(
                                        [
                                            ("type", "User"),
                                            (
                                                "url",
                                                "/services/data/v52.0/sobjects/User/{}".format(
                                                    self.sa_json["id"]
                                                ),
                                            ),
                                        ]
                                    ),
                                ),
                                ("Id", self.sa_json["id"]),
                                ("FirstName", self.sa_json["first_name"]),
                                ("LastName", self.sa_json["last_name"]),
                                ("Email", self.sa_json["email"]),
                            ]
                        )
                    ],
                ),
            ]
        )
        super().__init__(*args, **kwargs)

    def test_load(self):
        with mock.patch("simple_salesforce.api.SFType") as msf:
            mock_sf = msf.return_value
            mock_sf.User.get.return_value = self.mock_sa_json
            ss = SAService(mock_sf)
            s = ss.load(self.sa_json["id"])
            assert isinstance(s, SA)
            assert s.id == self.sa_obj.id
            assert s.first_name == self.sa_obj.first_name
            assert s.last_name == self.sa_obj.last_name
            assert s.email == self.sa_obj.email
        return True

    def test_search(self):
        with mock.patch("simple_salesforce.api.Salesforce") as msf:
            mock_sf = msf.return_value
            mock_sf.query.return_value = {"records": [self.mock_sa_json]}
            ss = SAService(mock_sf)
            rs = ss.search(first_name="Bruce")
            assert isinstance(rs[0], SA)
            assert rs == self.search_results
            rs = ss.search(last_name="Banner")
            assert rs == self.search_results
            rs = ss.search(first_name="Bruce", last_name="Banner")
            assert rs == self.search_results
            rs = ss.search(email="bbanner@gitlab.com")
            assert rs == self.search_results
        with self.assertRaises(ValueError):
            ss.search()
