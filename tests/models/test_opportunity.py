import json
from collections import OrderedDict
from pprint import pprint
from unittest import mock
from unittest import TestCase

from glsf.helper import json_to_ordered_dict
from glsf.models.opportunity import Opportunity
from glsf.models.opportunity import OpportunityService

# import simple_salesforce.api
# from glsf import models
# from glsf.sf_conn import SingletonSalesforce


# from src import glsf
# import models.user


class TestOpportunity(TestCase):
    def __init__(self, *args, **kwargs):
        self.mock_opportunity_json = {
            "id": "sado8f698a32r4uk",
            "name": "GitLab",
            "description": "GitLab, Inc.",
            "sal": "123",
            "sa": "321",
            "stage": "Closed",
            "is_closed": "True",
            "created_date": "2021-11-17T16:57:14.000+0000",
            "tech_eval_start_date": "",
            "tech_eval_end_date": "",
        }
        self.opportunity_obj = x = json.loads(
            json.dumps(self.mock_opportunity_json), object_hook=lambda d: Opportunity(**d)
        )
        self.mock_sf_opportunity = {
            "Id": self.mock_opportunity_json["id"],
            "Name": self.mock_opportunity_json["name"],
            "Description": self.mock_opportunity_json["description"],
            "OwnerId": self.mock_opportunity_json["sal"],
            "Solution_Architect__c": self.mock_opportunity_json["sa"],
            "StageName": self.mock_opportunity_json["stage"],
            "IsClosed": self.mock_opportunity_json["is_closed"],
            "CreatedDate": self.mock_opportunity_json["created_date"],
            "SA_Validated_Tech_Evaluation_Start_Date__c": self.mock_opportunity_json[
                "tech_eval_start_date"
            ],
            "SA_Validated_Tech_Evaluation_End_Date__c": self.mock_opportunity_json[
                "tech_eval_end_date"
            ],
        }
        self.search_results = [self.opportunity_obj]
        # self.sf_response_dict = json_to_ordered_dict(self.sf_response_json)

        self.mock_sf_response = OrderedDict(
            [
                (
                    "attributes",
                    OrderedDict(
                        [
                            ("type", "Opportunity"),
                            (
                                "url",
                                "/services/data/v52.0/sobjects/Opportunity/{}".format(
                                    self.mock_opportunity_json["id"]
                                ),
                            ),
                        ]
                    ),
                )
            ]
        )
        self.mock_sf_response.update(self.mock_sf_opportunity)

        super().__init__(*args, **kwargs)

    def test_load(self):
        with mock.patch("simple_salesforce.api.SFType") as msf:
            mock_sf = msf.return_value
            mock_sf.Opportunity.get.return_value = self.mock_sf_response
            os = OpportunityService(mock_sf)
            o = os.load(self.opportunity_obj.id)
            assert isinstance(o, Opportunity)
            for k, v in o.__dict__.items():
                assert v == getattr(self.opportunity_obj, k)

    def test_search(self):
        with mock.patch("simple_salesforce.api.Salesforce") as msf:

            mock_sf = msf.return_value
            mock_sf.query.return_value = {"records": [self.mock_sf_response]}

            os = OpportunityService(mock_sf)
            rs = os.search(name="GitLab")
            assert isinstance(rs[0], Opportunity)
            assert rs == self.search_results

            rs = os.search(stage="Closed")
            assert isinstance(rs[0], Opportunity)
            assert rs == self.search_results

            rs = os.search(sal_id=self.mock_opportunity_json["sal"])
            assert isinstance(rs[0], Opportunity)
            assert rs == self.search_results

            rs = os.search(sa_id=self.mock_opportunity_json["sa"])
            assert isinstance(rs[0], Opportunity)
            assert rs == self.search_results

            rs = os.search(sa_id=self.mock_opportunity_json["sa"], open=False)
            assert isinstance(rs[0], Opportunity)
            assert rs == self.search_results

            with self.assertRaises(ValueError):
                os.search(open=False)
                os.search()
