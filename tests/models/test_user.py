import json
import logging
from collections import OrderedDict
from unittest import mock
from unittest import TestCase

from glsf.models.user import User
from glsf.models.user import UserService

# import simple_salesforce.api
# from glsf import models
# from glsf.sf_conn import SingletonSalesforce

logger = logging.getLogger(__name__)

# from src import glsf
# import models.user


class TestUser(TestCase):
    def __init__(self, *args, **kwargs):
        self.user_json = {
            "id": "sado8f698a32r4uk",
            "first_name": "Bruce",
            "last_name": "Banner",
            "email": "bbanner@gitlab.com",
        }
        self.user_obj = x = json.loads(
            json.dumps(self.user_json), object_hook=lambda d: User(**d)
        )
        self.search_results = [self.user_obj]
        self.mock_sf_response = OrderedDict(
            [
                (
                    "attributes",
                    OrderedDict(
                        [
                            ("type", "User"),
                            (
                                "url",
                                "/services/data/v52.0/sobjects/User/{}".format(
                                    self.user_json["id"]
                                ),
                            ),
                        ]
                    ),
                ),
                ("Id", self.user_json["id"]),
                ("FirstName", self.user_json["first_name"]),
                ("LastName", self.user_json["last_name"]),
                ("Email", self.user_json["email"]),
            ]
        )
        # self.mock_query_results = OrderedDict(
        #     [
        #         ("totalSize", 1),
        #         ("done", True),
        #         (
        #             "records",
        #             [
        #                 OrderedDict(
        #                     [
        #                         (
        #                             "attributes",
        #                             OrderedDict(
        #                                 [
        #                                     ("type", "User"),
        #                                     (
        #                                         "url",
        #                                         "/services/data/v52.0/sobjects/User/{}".format(
        #                                             self.user_json["id"]
        #                                         ),
        #                                     ),
        #                                 ]
        #                             ),
        #                         ),
        #                         ("Id", self.user_json["id"]),
        #                         ("FirstName", self.user_json["first_name"]),
        #                         ("LastName", self.user_json["last_name"]),
        #                         ("Email", self.user_json["email"]),
        #                     ]
        #                 )
        #             ],
        #         ),
        #     ]
        # )
        super().__init__(*args, **kwargs)

    def test_load(self):
        with mock.patch("simple_salesforce.api.SFType") as msf:
            mock_sf = msf.return_value
            mock_sf.User.get.return_value = self.mock_sf_response
            us = UserService(mock_sf)
            u = us.load(self.user_json["id"])
            assert isinstance(u, User)
            for k, v in u.__dict__.items():
                assert v == getattr(self.user_obj, k)
        return True

    def test_search(self):
        with mock.patch("simple_salesforce.api.Salesforce") as msf:
            mock_sf = msf.return_value
            mock_sf.query.return_value = {"records": [self.mock_sf_response]}
            us = UserService(mock_sf)
            rs = us.search(first_name="Bruce")
            assert isinstance(rs[0], User)
            assert rs == self.search_results
            rs = us.search(last_name="Banner")
            assert isinstance(rs[0], User)
            assert rs == self.search_results
            rs = us.search(first_name="Bruce", last_name="Banner")
            assert isinstance(rs[0], User)
            assert rs == self.search_results
            rs = us.search(email="bbanner@gitlab.com")
            assert isinstance(rs[0], User)
            assert rs == self.search_results
        with self.assertRaises(ValueError):
            us.search()
