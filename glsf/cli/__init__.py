# https://github.com/getredash/redash/blob/master/redash/cli/__init__.py
"""
CLI tool to manage SalesForce in GitLab
"""
import click

from glsf.cli.opportunity import cli as opportunity_cli
from glsf.cli.sa import cli as sa_cli
from glsf.cli.sal import cli as sal_cli
from glsf.cli.user import cli as user_cli
from glsf.galactus import Galactus


@click.group("cli")
@click.option("--verbosity", "-v", count=True, default=0)
@click.version_option()
@click.pass_context
def cli(ctx, **kwargs):
    """Manager entrypoint for GitLab SalesForce CLI"""
    if kwargs.get("verbosity") > 2:
        click.echo("cli.__init__.cli kwargs => {}".format(kwargs))
    ctx.obj = Galactus(**kwargs)
    ctx.ensure_object(Galactus)


cli.add_command(user_cli, "user")
cli.add_command(sal_cli, "sal")
cli.add_command(sa_cli, "sa")
cli.add_command(opportunity_cli, "opp")
