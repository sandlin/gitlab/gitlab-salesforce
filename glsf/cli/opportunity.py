from collections import OrderedDict
from pprint import pprint

import click
from logzero import logger


#
# pd.set_option("display.max_columns", 100)
# pd.set_option("display.max_rows", 100)
# pd.set_option("display.width", None)
# All 3 of these print the same thing.
# pprint(pd.DataFrame(rs, columns=list(rs[0].keys())))
# pprint(pd.DataFrame.from_records(rs))
# pprint(pd.DataFrame.from_dict(sa_list))


@click.group("cli")
@click.pass_context
def cli(ctx):
    """User entrypoint group"""
    pass


@cli.command()
@click.option("--id", help="The ID of the Opportunity", required=True)
@click.pass_obj
def load(obj, id):
    try:
        s = obj.opportunity.load(id)
        pprint(s)
    except Exception as e:
        raise e


@cli.command(name="search", help="Search opportunities")
@click.option("--sa", help="Assigned Solution Architect's SF ID")
@click.option("--sal", help="Assigned SAL's SF ID")
@click.option("--name", help="Name of Opportunity")
@click.option("--stage", help="Stage of Opportunity")
@click.pass_obj
def search(obj, sa: str = None, sal: str = None, name: str = None, stage: str = None):
    if sa is None and sal is None and name is None and stage is None:
        raise Exception("You must provide at least one of: sa, sal, name, stage.")
    try:
        s = obj.opportunity.search(name=name, stage=stage, sal_id=sal, sa_id=sa)
        pprint(s)
    except Exception as e:
        raise e


#
#
# @cli.command(name="search", help="Search for Opportunity")
# @click.option("-", "--first_name", default=None, help="Search for this first name.")
# @click.option("-l", "--last_name", default=None, help="Search for this last name.")
# @click.option("-e", "--email", default=None, help="Search for this email address.")
# @click.pass_obj
# def search(obj, first_name, last_name, email) -> OrderedDict:
#     """
#     Search for User via first and/or last name
#     TODO: Merge this method between user + sal + sa. Currently duplicate code.
#     :param first_name: First Name of User
#     :param last_name: Last Name of User
#     :param email: Email of User
#     :return query_results: User Search Results
#     """
#     if first_name is None and last_name is None and email is None:
#         click.echo("-" * 20)
#         click.echo("You must provide at least one of: first_name, last_name, email.")
#         click.echo("-" * 20)
#         with click.Context(search) as ctx:
#             click.echo(search.get_help(ctx))
#         exit()
#     try:
#         sa_list = obj.sa.search(first_name, last_name, email)
#         for s in sa_list:
#             click.echo(s.to_json())
#
#     except Exception as e:
#         click.echo(cli.get_help(obj))
#         raise e
