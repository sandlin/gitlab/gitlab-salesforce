from collections import OrderedDict
from pprint import pprint

import click
from logzero import logger


@click.group("cli")
@click.pass_context
def cli(ctx):
    """User entrypoint group"""
    pass


@cli.command()
@click.pass_obj
@click.option("--id", help="The ID of the User")
def load(obj, id):
    try:
        s = obj.user.load(id)
        pprint(s)
    except Exception as e:
        raise e


@cli.command(name="search", help="Search for User")
@click.option("-f", "--first_name")
@click.option("-l", "--last_name")
@click.option("-e", "--email")
@click.pass_obj
def search(
    galactus, first_name: str = None, last_name: str = None, email: str = None
) -> OrderedDict:
    """
    Search for User via first and/or last name
    TODO: Merge this method between user + sal + sa. Currently duplicate code.
    :param first_name: First Name of User
    :param last_name: Last Name of User
    :param email: Email of User
    :return query_results: User Search Results
    """
    if first_name is None and last_name is None and email is None:
        raise Exception("You must provide at least one of: first_name, last_name, email.")
    try:
        user_list = galactus.user.search(first_name, last_name, email)
        # All 3 of these print the same thing.
        # pprint(pd.DataFrame(rs, columns=list(rs[0].keys())))
        # pprint(pd.DataFrame.from_records(rs))
        # pprint(pd.DataFrame.from_dict(sa_list))
        for u in user_list:
            print(u.to_json())
    except Exception as e:
        raise e
