from collections import OrderedDict
from pprint import pprint

import click
import pandas as pd
from logzero import logger

from glsf.models.account import Account
from glsf.models.sal import SALService


@click.group("cli")
@click.pass_context
def cli(ctx):
    """User entrypoint group"""
    pass


@cli.command()
@click.pass_obj
@click.option("--id", help="The ID of the SAL")
def load(galactus, id):
    try:
        s = galactus.sal.load(id)
        pprint(s)
    except Exception as e:
        raise e


@cli.command(name="search", help="Search for SAL")
@click.option("-f", "--first_name")
@click.option("-l", "--last_name")
@click.option("-e", "--email")
@click.pass_obj
def search(
    galactus, first_name: str = None, last_name: str = None, email: str = None
) -> OrderedDict:
    """
    Search for User via first and/or last name
    TODO: Merge this method between user + sal + sa. Currently duplicate code.
    :param first_name: First Name of User
    :param last_name: Last Name of User
    :param email: Email of User
    :return query_results: User Search Results
    """
    if first_name is None and last_name is None and email is None:
        raise Exception("You must provide at least one of: first_name, last_name, email.")
    try:
        sal_list = galactus.sal.search(first_name=first_name, last_name=last_name, email=email)
        # All 3 of these print the same thing.
        # pprint(pd.DataFrame(rs, columns=list(rs[0].keys())))
        # pprint(pd.DataFrame.from_records(rs))
        # pprint(pd.DataFrame.from_dict(sa_list))
        for s in sal_list:
            print(s.to_json())
    except Exception as e:
        raise e


@cli.command(name="accounts")
@click.option("--id", help="The ID of the SAL")
@click.pass_context
def accounts(ctx, id):
    s = Account.get_via_sal_id(ctx.obj.sf, id)
    # #s.load_accounts()
    # for k, v in s.accounts.items():
    #     pprint(v.__dict__)
    # pprint(a.__dict__)


# Make sal an entry point.
