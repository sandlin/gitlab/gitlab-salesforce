from typing import List
from typing import Union

from logzero import logger

from glsf.models.sf_object import SfObject
from glsf.models.sf_object import SfService
from glsf.sf_conn import SingletonSalesforce


class User(SfObject):
    def __init__(self, **kwargs):
        """
        Compute represents a user object in SalesForce

        :param kwargs: id, first_name, last_name, email
        :type kwargs: dict

        """
        super().__init__(**kwargs)
        # self.id = kwargs.get("Id") if "Id" in kwargs else ""
        # self.first_name = kwargs.get("FirstName") if "FirstName" in kwargs else ""
        # self.last_name = kwargs.get("LastName") if "LastName" in kwargs else ""
        # self.email = kwargs.get("Email") if "Email" in kwargs else ""

        if "id" in kwargs:
            self.id = kwargs.get("id")
        if "first_name" in kwargs:
            self.first_name = kwargs.get("first_name")
        if "last_name" in kwargs:
            self.last_name = kwargs.get("last_name")
        if "email" in kwargs:
            self.email = kwargs.get("email")

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, User):
            return NotImplemented
        return (
            self.id == o.id
        )  # and self.first_name == o.first_name and self.last_name == o.last_name and self.email == o.email

    def __ne__(self, o: object) -> bool:
        if not isinstance(o, User):
            return NotImplemented
        return not (self.id == o.id)


class UserService(SfService):
    def __init__(self, service_cls=None, **kwargs):
        """
        Manages user in SF
        :param sfconn: SalesForce client
        :type sfconn: SingletonSalesforce
        :param kwargs:
        """
        sc = service_cls if service_cls else "User"
        #sc = kwargs.get("service_cls") if "service_cls" in kwargs else "User"
        super().__init__(service_cls = sc, **kwargs)

    def load(self, id: str) -> User:
        """
        Load User from SalesForce via ID.

        """
        self.log.debug(f"UserService.load({id})")
        if id is None:
            self.log.error("Please provide a id")
            return None

        rs = self.sfconn.User.get(id)
        user = User(id=id)
        user.email = rs["Email"]
        user.first_name = rs["FirstName"]
        user.last_name = rs["LastName"]
        self.log.debug(user)
        self.log.debug(f"END UserService.load({user.id})")
        return user

    def search(
        self, first_name: str = None, last_name: str = None, email: str = None
    ) -> List[User]:
        if first_name is None and last_name is None and email is None:
            raise ValueError(
                "first_name and/or last_name and/or email must be provided as args"
            )
        query = "SELECT Id, FirstName, LastName, Email FROM User WHERE "
        query = (
            query + f"FirstName LIKE '{first_name}'" if (first_name is not None) else query
        )
        query = (
            query + " AND " if (first_name is not None) and (last_name is not None) else query
        )
        query = query + f"LastName LIKE '{last_name}'" if last_name is not None else query
        query = (
            query + " AND "
            if (first_name is not None) and (last_name is not None) and (email is not None)
            else query
        )
        query = query + f"Email LIKE '{email}'" if (email is not None) else query
        self.log.debug(query)
        query_results = self.sfconn.query(query)
        rs = query_results["records"]
        to_return = []

        for r in rs:
            u = User(id=r["Id"])
            for k, v in r.items():
                setattr(u, k, v)
            to_return.append(u)
        return to_return
