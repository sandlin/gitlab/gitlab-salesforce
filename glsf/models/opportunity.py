from typing import List

from logzero import logger

from glsf.helper import json_serial
from glsf.models.sf_object import SfObject
from glsf.models.sf_object import SfService


class Opportunity(SfObject):
    def __init__(self, *args, **kwargs):
        self.__dict__.update(kwargs)
        super().__init__()

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Opportunity):
            return NotImplemented
        return (
            self.id == o.id
            and self.name == o.name
            and self.description == o.description
            and self.sal == o.sal
            and self.sa == o.sa
        )

    def __ne__(self, o: object) -> bool:
        if not isinstance(o, Opportunity):
            return NotImplemented
        return not (self.id == o.id)


class OpportunityService(SfService):
    def __init__(self, sfconn, **kwargs):
        """
        Manages Opportunity in SF
        :param sfconn: SalesForce client
        :type sfconn: SingletonSalesforce
        :param kwargs:
        """
        sc = kwargs.get("service_cls") if "service_cls" in kwargs else "Opportunity"
        super().__init__(service_cls=sc, **kwargs)
        #super().__init__(sfconn, sc, **kwargs)

    def load(self, id):
        """
        Load opportunity from SF.
        :param id: The account's Salesforce ID
        :return Opportunity: The account object loaded.
        """
        rs = self.sfconn.Opportunity.get(id)
        self.log.debug(rs)
        return self.rs_to_object(rs)

    def rs_to_object(self, rs):
        o = Opportunity(id=rs["Id"])
        o.name = rs["Name"]
        o.description = rs["Description"]
        o.sa = rs["Solution_Architect__c"]
        o.sal = rs["OwnerId"]
        o.stage = rs["StageName"]
        o.is_closed = rs["IsClosed"]
        o.created_date = rs["CreatedDate"]
        o.tech_eval_start_date = rs["SA_Validated_Tech_Evaluation_Start_Date__c"]
        o.tech_eval_end_date = rs["SA_Validated_Tech_Evaluation_End_Date__c"]
        return o

    def object_to_rs(self, o):
        rs = {}
        rs["id"] = o.id
        rs["Name"] = o.name
        rs["Description"] = o.description
        rs["Solution_Architect__c"] = o.sa
        rs["OwnerId"] = o.sal
        rs["StageName"] = o.stage
        rs["IsClosed"] = o.is_closed
        rs["CreatedDate"] = o.created_date
        rs["SA_Validated_Tech_Evaluation_Start_Date__c"] = o.tech_eval_start_date
        rs["SA_Validated_Tech_Evaluation_End_Date__c"] = o.tech_eval_end_date
        return rs

    def search(
        self,
        name: str = None,
        stage: str = None,
        sal_id: str = None,
        sa_id: str = None,
        open: bool = True,
    ) -> List[Opportunity]:
        """
        Search for opportunities which match a specific filter.
        Filter will be passed as arg.
        :param name:
        :param stage:
        :param sal:
        :param sa:
        :return:
        """
        if name is None and stage is None and sal_id is None and sa_id is None:
            raise ValueError(
                "One of the following args must be provided: name, stage, sal_id, sa_id, open"
            )

        query = (
            "SELECT Id, Name, Description, StageName, CreatedDate, IsClosed, "
            "OwnerId, Solution_Architect__c, "
            "SA_Validated_Tech_Evaluation_Start_Date__c, SA_Validated_Tech_Evaluation_End_Date__c "
            "FROM Opportunity WHERE Id != Null "
        )
        if name is not None:
            query = query + f" AND Name LIKE '{name}'"
        if stage is not None:
            query = query + f" AND StageName LIKE '{stage}'"
        if sal_id is not None:
            query = query + f" AND OwnerId LIKE '{sal_id}'"
        if sa_id is not None:
            query = query + f" AND Solution_Architect__c = '{sa_id}'"
        if open is not None:
            query = query + f" AND IsClosed = '{not open}'"

        query_results = self.sfconn.query(query)
        rs = query_results["records"]
        to_return = []
        for r in rs:
            o = self.rs_to_object(r)
            to_return.append(o)
        return to_return
