import json

from logzero import logger

from glsf.helper import json_serial


class SfObject:
    def __init__(self, **kwargs):
        """
        Base object for common methods and to provide a standard interface for constructing objects
        from M3Responses

        :param kwargs: Not used by base class
        :type kwargs: dict
        """
        self.log = logger

    def to_dict(self) -> dict:
        """
        Dumps this object to dict

        :return: object as dict
        :rtype: dict
        """
        return {k: v for k, v in self.__dict__.items() if not k.startswith("__")}

    def to_json(self) -> str:
        """
        Dumps this object to json

        :return: object as json
        :rtype: str
        """
        return json.dumps(self.to_dict(), indent=2, default=json_serial)

    def __repr__(self):
        return f"{self.__class__} " + " ".join(
            [f"{k}={v}" for k, v in self.to_dict().items()]
        )

    def pretty(self) -> str:
        """
        Shortcut to self.to_json() for now.

        :return: object as json
        :rtype: str
        """
        return self.to_json()

    def short(self) -> str:
        """
        Return short representation of this object

        :return: self.name as string
        :rtype: str
        """
        name = getattr(self, "name", "unknown")
        return name


class SfService:
    def __init__(self, service_cls = None, **kwargs):
        # def __init__(self, sfconn, service_cls=None, **kwargs):
        """
        Base class for services

        :param sfconn: authenticated client
        :type sfconn: SalesForce connection
        :param service_cls: Base object this service returns
        :type service_cls: Any
        :param kwargs: extra meta data or arguments for a specific service
        :type kwargs: dict
        """
        self.service_cls = service_cls if service_cls else "SfService"
        self.sfconn = kwargs.get("sfconn")
        self.log = logger
