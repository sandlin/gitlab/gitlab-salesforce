from logzero import logger

from glsf.models.user import User
from glsf.models.user import UserService


class SAL(User):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.accounts = {}

    #
    # def reprJSON(self):
    #     try:
    #         u = super().reprJSON()
    #         u.update(dict(accounts=[lambda a: a.__str__ for a in self.accounts]))
    #         return u
    #     except:
    #         return dict()


class SALService(UserService):
    def __init__(self, **kwargs):
        """
        Manages user in SF
        :param sfconn: SalesForce client
        :type sfconn: SingletonSalesforce
        :param kwargs:
        """
        super().__init__(service_cls="SAL", **kwargs)

    def search(self, **kwargs):
        users = super().search(**kwargs)
        to_return = []
        for u in users:
            s = SAL(**u.__dict__)
            to_return.append(s)
        return to_return

    def load(self, id: str) -> SAL:
        user = super().load(id)
        sal = SAL(**user.__dict__)
        return sal
