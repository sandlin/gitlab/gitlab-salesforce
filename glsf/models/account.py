import json

from logzero import logger
from simple_salesforce import Salesforce

from glsf.models.sf_object import SfObject
from glsf.models.sf_object import SfService

# from glsf.models import sfconn
# from .user import User
# from .sal import SAL


class Account(SfObject):
    def __init__(self, *args, **kwargs):
        self.__dict__.update(kwargs)
        self.id = None
        self.name = None
        self.description = None
        self.license_count = None
        self.license_tier = None
        self.owner = None
        self.sa = None
        super().__init__()
    #
    #     super().__init__()

    #     self.id = kwargs.get("Id") if "Id" in kwargs else None
    #     self.name = kwargs.get("Name") if "Name" in kwargs else None
    #     self.description = kwargs.get("Description") if "Description" in kwargs else None
    #     self.license_count = kwargs.get("LicenseCount") if "LicenseCount" in kwargs else None
    #     self.license_tier = kwargs.get("LicenseTier") if "LicenseTier" in kwargs else None
    #     self.owner = kwargs.get("Owner") if "Owner" in kwargs else None
    #     self.sa = kwargs.get("SA") if "SA" in kwargs else None
    #
    # #
    # def reprJSON(self):
    #     try:
    #         return dict(
    #             id=self.id,
    #             name=self.name,
    #             description = self.description,
    #             license_count = self.license_count,
    #             tier = self.license_tier,
    #             owner=self.owner.reprJSON(),
    #             sa=self.sa.reprJSON(),
    #         )
    #     except:
    #         return dict()
    #
    # def __str__(self):
    #     return json.dumps(self.reprJSON())
    #
    # def __repr__(self):
    #     return self.__str__()

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Account):
            return NotImplemented
        return (
            self.id == o.id
            and self.name == o.name
            and self.description == o.description
            and self.sal == o.sal
            and self.sa == o.sa
            and self.license_count == o.license_count
            and self.license_tier == o.license_tier
        )

    def __ne__(self, o: object) -> bool:
        if not isinstance(o, Account):
            return NotImplemented
        return not (
            self.id == o.id
            and self.name == o.name
            and self.description == o.description
            and self.sal == o.sal
            and self.sa == o.sa
            and self.license_count == o.license_count
            and self.license_tier == o.license_tier
        )


class AccountService(SfService):
    def __init__(self, sfconn, **kwargs):
        """
        Manages Account in SF
        :param sfconn: SalesForce client
        :type sfconn: SingletonSalesforce
        :param kwargs:
        """
        sc = kwargs.get("service_cls") if "service_cls" in kwargs else "Account"
        super().__init__(service_cls=sc, **kwargs)

    #
    # @classmethod
    # def load_via_sal_id(cls, sfconn, sal_id):
    #     try:
    #         a = sfconn.Account.select().filter("OwnerId = '{}'".format(sal_id))
    #         print(a)
    #     except Exception as e:
    #         log.exception(e)
    #         raise(e)
    # #
    # def load_accounts(self):
    #     qr = sfconn.query_all("SELECT Id FROM Account WHERE OwnerId = '{sal_id}'".format(sal_id=self.id))['records']
    #     for r in qr:
    #         self.accounts[r['Id']] = account.Account(r['Id'])
    #         self.accounts[r['Id']].
    #         self.accounts[r['Id']].load_via_id(sfconn)


    def load(self, id: str):
        """
        Load account from SF.
        :param id: The account's Salesforce ID
        :return Account: The account object loaded.
        """
        rs = self.sfconn.Account.get(id)

        a = Account(Id=id)
        a.name = rs["Name"]
        a.description = rs["Description"]
        a.license_count = rs["Number_of_Licenses_Total__c"]
        a.license_tier = rs["Products_Purchased__c"]
        a.owner = rs["OwnerId"]
        a.sa = rs["Solutions_Architect_Lookup__c"]
        return a
        # a.owner = glsf.models.sal.SAL()
        # self.owner.id=a['OwnerId']
        # self.owner.load_via_id(sfconn)
        # sfa = sfconn.User.get(self.id)
        # self.owner.load_via_id(sfconn)
        # self.sa = User(id=a['Solutions_Architect_Lookup__c'], sfconn=sfconn)
        # self.sa.load_via_id(sfconn)


#
#
# "0014M00001wUz3mQAC"
# def __init__(self):
# cfg = SFConfig.SFConfig()
# self.sf = Salesforce(username=cfg.username, password=cfg.password, instance=cfg.url, security_token=cfg.token)
