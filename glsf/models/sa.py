import json

from logzero import logger

from glsf.models.opportunity import OpportunityService
from glsf.models.user import User
from glsf.models.user import UserService


class SA(User):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.log.debug(f"__init__({kwargs})")
        self.accounts = {}
        self.opps = {}

    #
    # def reprJSON(self):
    #     try:
    #         u = super().reprJSON()
    #         u.update(dict(accounts=[lambda a: a.__str__ for a in self.accounts]))
    #         return u
    #     except:
    #         return dict()


class SAService(UserService):
    def __init__(self, sfconn, **kwargs):
        """
        Manages user in SF
        :param sfconn: SalesForce client
        :type sfconn: SingletonSalesforce
        :param kwargs:
        """
        #super().__init__(sfconn, **dict(service_cls="SA", **kwargs))
        super().__init__(service_cls="SA", **kwargs)

    def load(self, id: str):
        user = super().load(id)
        self.log.debug(user.__dict__)
        s = SA(**user.__dict__)
        self.log.debug(s)
        return s

    def search(self, **kwargs):
        users = super().search(**kwargs)
        to_return = []
        for u in users:
            s = SA(**u.__dict__)
            to_return.append(s)
        return to_return

    def load_opportunities(self, sa: SA, open: bool = True):
        os = OpportunityService
        ops = os.search(sa_id=sa.id, open=open)
