import configparser
from os import path

from logzero import logger
from logzero import loglevel

from .helper import map_log_level
from glsf.models.account import AccountService
from glsf.models.opportunity import OpportunityService
from glsf.models.sa import SAService
from glsf.models.sal import SALService
from glsf.models.user import UserService
from glsf.sf_conn import SingletonSalesforce


class Galactus:
    def __init__(self, verbosity):
        """
        Primary entry-point for GLSF (GitLab-SalesForce). Reads SF auth config, sets up authenticated Client, holds references to each service.

        :param verbosity: logging level. 0 to 3, ERROR to DEBUG
        :type verbosity: int
        """

        self.log_level = verbosity
        self.sf_creds = {}
        self._load_config()

        desired_level = map_log_level(verbosity)
        loglevel(desired_level, update_custom_handlers=True)
        self.log = logger

        self.sfconn = SingletonSalesforce(self.sf_creds)

        self.user = UserService(sfconn=self.sfconn.sf, logger=logger)
        self.sal = SALService(sfconn=self.sfconn.sf, logger=logger)
        self.sa = SAService(sfconn=self.sfconn.sf, logger=logger)

        self.opportunity = OpportunityService(self.sfconn.sf, logger=logger)
        self.account = AccountService(self.sfconn.sf, logger=logger)

    #
    # @property
    # def log_level(self):
    #     return self.log_level

    def _load_config(self, config_file: str = None) -> None:
        """
        Load SalesForce auth config from file.
        :return:
        """
        self.config = {}
        if config_file is None:
            config_file = path.join(path.expanduser("~"), ".salesforce")
        cp = configparser.ConfigParser()
        try:
            cp.read(config_file)
            if "salesforce" not in cp.sections():
                raise Exception("Your ~/.salesforce file cannot be loaded.")
            self.config = cp["salesforce"]
            if "Galactus" in cp.sections():
                if self.log_level is None:
                    self.log_level = cp.get("Galactus", "LOG_LEVEL", fallback="WARNING")
            for option in cp["salesforce"]:
                self.sf_creds[option] = cp.get("salesforce", option)

            # c.SF_USERNAME = cp.get("salesforce", "SF_USERNAME")
            # c.SF_PASSWORD = cp.get("salesforce", "SF_PASSWORD")
            # c.SF_TOKEN = cp.get("salesforce", "SF_TOKEN")
            # c.SF_INSTANCE = cp.get("salesforce", "SF_INSTANCE")
            # c.SF_USERNAME = cp.get('salesforce', 'SF_USERNAME', fallback='')
            # c.SF_PASSWORD = cp.get('salesforce', 'SF_PASSWORD', fallback='')
            # c.SF_TOKEN = cp.get('salesforce', 'SF_TOKEN', fallback='')
            # c.SF_INSTANCE = cp.get('salesforce', 'SF_INSTANCE', fallback='') #fallback='gitlab.salesforce.com')

        except configparser.NoSectionError as e:
            raise Exception("Unable to find salesforce in your config file.")
