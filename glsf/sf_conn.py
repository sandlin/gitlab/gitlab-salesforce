# https://github.com/rajeshkaushik/flask-simple-salesforce/blob/master/flask_simple_salesforce/api.py
from logzero import logger
from requests.exceptions import ConnectionError
from simple_salesforce import Salesforce
from simple_salesforce import SalesforceAuthenticationFailed
from simple_salesforce.exceptions import SalesforceExpiredSession
from zeep import Client
from zeep.transports import Transport

from glsf.helper import map_log_level


def get_sf_object(sfcfg):
    try:
        sf_obj = Salesforce(
            username=sfcfg["sf_username"],
            password=sfcfg["sf_password"],
            security_token=sfcfg["sf_token"],
            instance=sfcfg["sf_instance"],
        )
        return sf_obj
    except SalesforceAuthenticationFailed as e:
        logger.warning("You are not connected to SalesForce. Check your Config.")
        return None


def retry_on_session_expire(func):
    """
    Decorator to retry once on SalesforceExpiredSession or ConnectionError.
    """

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except SalesforceExpiredSession as e:
            # update the sf instance for one retry
            SingletonSalesforce.update_sf_obj()
            return func(*args, **kwargs)
        except ConnectionError:
            try:
                return func(*args, **kwargs)
            except SalesforceExpiredSession as e:
                # update the sf instance for one retry
                SingletonSalesforce.update_sf_obj()
                return func(*args, **kwargs)

    return wrapper


class SingletonSalesforce:
    def __new__(cls, cfg):
        if not hasattr(cls, "_instance"):
            sf_obj = get_sf_object(cfg)
            cls._instance = super().__new__(cls)
            cls._instance.sf = sf_obj
            cls._instance.WSDL_METHODS = set()
        return cls._instance

    def get_wsdl_service(self, wsdl_path):
        domain = (
            self._sfcfg.SF_INSTANCE
        )  # current_app.config['SF_DOMAIN'] + '.salesforce.com'
        wsdl = f"https://{domain}{wsdl_path}"

        if not hasattr(self, wsdl_path):
            # Add cookie `sid` to get WSDL
            sid = self.sf.session_id
            self.sf.session.cookies["sid"] = sid
            client = Client(wsdl, transport=Transport(session=self.sf.session))

            # Add SessionHeader for accessing the service
            client.set_default_soapheaders({"SessionHeader": sid})

            setattr(self, wsdl_path, client)
            self.WSDL_METHODS.add(wsdl_path)

        return getattr(self, wsdl_path).service

    @classmethod
    def destroy_instance(cls):
        """
        delete engage instance in case of Salesforce token expires
        """
        if hasattr(cls, "_instance"):
            del cls._instance

    @classmethod
    def update_sf_obj(cls):
        """refresh the SF instance reference on engage class"""

        if hasattr(cls, "_instance"):
            sf_obj = get_sf_object()
            cls._instance.sf = sf_obj
            for client_attr in cls._instance.WSDL_METHODS:
                if hasattr(cls._instance, client_attr):
                    delattr(cls._instance, client_attr)
