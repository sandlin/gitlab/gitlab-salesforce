#!/usr/bin/env python
import datetime
import json
import logging
import re
from collections import OrderedDict
from typing import List

# from .error import BadDataError

LOG_LEVELS = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]


def map_log_level(flag):
    """
    Map verbosity int to log level
    :param flag: verbosity count
    :type flag: int
    :return: log level mapped
    :rtype: int
    """
    if isinstance(flag, bool):
        flag = -1 if flag else 0
    try:
        level = LOG_LEVELS[flag]
    except IndexError:
        try:
            level = LOG_LEVELS[f"logging.{flag}"]
        except IndexError:
            level = LOG_LEVELS[-1]
        except TypeError:
            level = LOG_LEVELS[-1]
    return level


def to_string(iterator: List[str]) -> str:  # pragma: no cover
    """
    Shortcut to join list to comma separated string

    :param iterator: list of strings
    :type iterator: List[str]
    :return: comma separated string
    :rtype: str
    """
    return ",".join(iterator)


def to_list(string: str) -> List[str]:
    """
    Split CSV string to list

    :param string: csv string
    :type string: str
    :return: list of strings
    :rtype: List[str]
    """
    return string.split(",")


def json_to_ordered_dict(json_str):
    """
    Convert JSON string to OrderedDict.
    :param json_str: A JSON string
    :return: OrderedDict reporesenting the JSON string.
    """
    print(json.JSONDecoder(object_pairs_hook=OrderedDict).decode(json.dumps(json_str)))
    return json.JSONDecoder(object_pairs_hook=OrderedDict).decode(json.dumps(json_str))


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    try:
        return obj.__dict__
    except Exception:
        raise TypeError("Type %s not serializable" % type(obj))
