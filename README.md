# GLSF - GitLab SalesForce

![Project Stage][project-stage-shield]
![Pipeline Status][gitlab-pipeline-status]
![Maintenance][maintenance-shield]
![coverage report][gitlab-coverage-report]
![Latest Release][gitlab-release-svg]

[![MIT License][license-shield]][license-url]
[![Maintaner][maintainer-shield]]([maintainer-url])

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Coverage Details](https://sandlin.gitlab.io/gitlab/gitlab-salesforce/)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)


<!-- ABOUT THE PROJECT -->
## About The Project
GLSF is a command line tool for managing Salesforce data within GitLab.
The target Audience is currently just Solution Architects, as this is my role. However, this can easily be expanded to other user groups.

### Use Cases:
1. As a _Solution Architect_, I want the ability to maintain the accounts and opportunities of which I am to be assigned.
2. As a _TAM_, I want the ability to maintain the accounts and opportunities of which I am to be assigned.
3. As a _SAL_, I want the ability to maintain the accounts and opportunities of which I am to be assigned.
4. ...

### Built With

* [simple-salesforce](https://simple-salesforce.readthedocs.io/en/latest/)
* [Tox](https://tox.wiki/en/latest/index.html)
* [Poetry](https://python-poetry.org/)


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* __SalesForce Credential File__
  1. Get your `password` and `security token` from SalesForce.
     1. [What is Salesforce Security Token and How Do I Find It?](https://www.mcafee.com/blogs/enterprise/cloud-security/what-is-salesforce-security-token-and-how-do-i-find-it/
  2. Copy the [template file](./templates/.salesforce) to your home directory.
     1. `cp ./templates/salesforce ~/.salesforce`
  3. Update the config file `~/.salesforce` with your `email`, `password`, and `security token`



### Installation
1. __GitLab Token__
   * Get your [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) from GitLab.
2. __Install glsf__
   * ```pip install gitlab-salesforce --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/34361332/packages/pypi/simple```


<!-- USAGE EXAMPLES -->
## Usage
```glsf --help```

<!-- ROADMAP -->
## Roadmap
See the [open issues](https://gitlab.com/jsandlin/gitlab/gitlab-salesforce/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing
* Poetry
  ```sh
  brew install poetry
  ```
1. Clone the repo
   ```sh
   ➜ git clone https://gitlab.com/jsandlin/gitlab/gitlab-salesforce.git
   ```
1. Initialize your `pipenv`
    ```shell script
    ➜ pipenv install
    ```
1. Activate your project's virtualenv
    ```shell script
    ➜ pipenv shell
    ```


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

James Sandlin - [LinkedIn](https://www.linkedin.com/in/jamessandlin/) - jsandlin@gitlab.com

Project Link: [https://gitlab.com/jsandlin/gitlab/gitlab-salesforce](https://gitlab.com/jsandlin/gitlab/gitlab-salesforce)




<!-- Let's define some variables for ease of use. -->
[gitlab-coverage-report]: https://gitlab.com/sandlin/gitlab/gitlab-salesforce/badges/groot/coverage.svg
[gitlab-pipeline-status]: https://gitlab.com/sandlin/gitlab/gitlab-salesforce/badges/groot/pipeline.svg
[gitlab-release-svg]: https://gitlab.com/sandlin/gitlab/gitlab-salesforce/-/badges/release.svg
[project-stage-shield]: https://img.shields.io/badge/project%20stage-development-yellowgreen.svg
[license-shield]: https://img.shields.io/github/license/hassio-addons/repository-beta.svg


[issues]: https://gitlab.com/sandlin/gitlab/gitlab-salesforce/issues
[maintenance-shield]: https://img.shields.io/maintenance/yes/2022.svg
[maintainer-shield]: https://img.shields.io/badge/maintainer-James_Sandlin-blueviolet
[maintainer-url]: https://gitlab.com/jsandlin



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/badge/gitlab/all-contributors/jsandlin/gitlab/gitlab_salesforce/groot?style=flat-square
[contributors-url]: https://gitlab.com/jsandlin/repo/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/jsandlin/repo.svg?style=flat-square
[forks-url]: https://gitlab.com/jsandlin/repo/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/jsandlin/repo.svg?style=flat-square
[stars-url]: https://gitlab.com/jsandlin/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/jsandlin/repo.svg?style=flat-square
[issues-url]: https://gitlab.com/jsandlin/repo/issues
[license-shield]: https://img.shields.io/gitlab/license/jsandlin/repo.svg?style=flat-square
[license-url]: https://gitlab.com/jsandlin/repo/blob/master/LICENSE.txt
[product-screenshot]: images/screenshot.png
